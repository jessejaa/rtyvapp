﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace RtyvApp.Droid
{
    [Activity(Label = "RtyvApp - Splash", Theme = "@style/MyTheme.Splash", MainLauncher = true, NoHistory = true)]
    public class SplashActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }

        // OnResume ajetaan aina ->
        protected override void OnResume()
        {
            base.OnResume();
            Task startupWork = new Task(() => { StartApp(); });
            startupWork.Start();
        }
        
        async void StartApp()
        {
            await Task.Delay(2500); // Säie hidastaa splashruudun poistumista
            StartActivity(new Intent(Application.Context, typeof(MainActivity)));
        }

        // Ei tee mitään jos painetaan back -buttonia
        public override void OnBackPressed() { }
    }
}