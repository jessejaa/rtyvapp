﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using RtyvApp.Interfaces;

[assembly: Xamarin.Forms.Dependency(typeof(RtyvApp.Droid.LocalDB))]
namespace RtyvApp.Droid
{
    public class LocalDB : ILocalDB
    {
        // Paikallinen tietokanta
        public string DBConn(string filename)
        {
            // Haetaan android käyttäjän henkilökohtainen kansio
            string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            // Palautetaan em. hakemistopolku
            return Path.Combine(path, filename);
        }
    }
}