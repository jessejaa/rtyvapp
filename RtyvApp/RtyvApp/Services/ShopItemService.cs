﻿using RtyvApp.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RtyvApp.Services
{
    public class ShopItemService
    {
        private SQLiteAsyncConnection db;

        public ShopItemService(string path)
        {
            db = new SQLiteAsyncConnection(path);

            // Taulun luominen
            db.CreateTableAsync<ShopItem>().Wait();
            
        }

        public async Task<int> AddShopItemAsync(ShopItem item)
        {
            if (await db.FindAsync<ShopItem>(x => x.Id.Equals(item.Id)) != null)
            {
                Console.WriteLine("DB: Updating a listing");
                // Listauksen päivittäminen
                return await db.UpdateAsync(item);
            }
            else
            {
                Console.WriteLine("DB: Creating a listing");
                // Listauksen lisääminen tietokantaan
                return await db.InsertAsync(item);
            }
        }

        public async Task<int> DelShopItemAsync(ShopItem item)
        {
            return await db.DeleteAsync(item);
        }

        public async Task<int> ClearTableAsync()
        {
            return await db.ExecuteAsync("DELETE FROM ShopItem");
        }
        public async Task<List<ShopItem>> GetShopItemsAsync()
        {
            return await db.Table<ShopItem>().ToListAsync();
        }

        public async Task<List<ShopItem>> GetShopItemsAsync(int userid)
        {
            return await db.Table<ShopItem>().Where(i => i.SellerID == userid).ToListAsync();
        }


        public async Task<ShopItem> GetShopItemAsync(int id)
        {
            return await db.Table<ShopItem>().Where(i => i.Id == id).FirstOrDefaultAsync();
        }

        public async Task<List<ShopItem>> GetShopItemsByUserAsync(User user)
        {
            return await db.Table<ShopItem>().Where(x => x.SellerID == user.Id).ToListAsync();
        }

        public async Task<int> GetNextIdAsync()
        {
            // Haetaan käyttäjät
            List<ShopItem> items = await GetShopItemsAsync();

            // Haetaan käyttäjien identifikaatit integereinä
            List<int> ids = items.Select(x => x.Id).ToList();

            // Laskijan määritys
            int counter = 0;
            if (ids.Count() > 0)
            {
                counter = ids.First();
            }
            else
            {
                counter = counter - 1;
            }

            while (counter < 999)
            {
                // Jos id lista ei sisällä laskijaa
                if (!ids.Contains(counter++))
                {
                    // Palautetaan laskija (joka ei ole listassa)
                    return counter;
                }
            }

            // Palautetaan joku erroriksi merkitty numero vaikka koska en jaksa muuttaa tietokantaa hyväksymään nulleja :D
            return 404;

        }
    }
}
