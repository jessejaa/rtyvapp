﻿using RtyvApp.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RtyvApp.Services
{

    public class UserService
    {
        private SQLiteAsyncConnection db;

        public UserService(string path)
        {
            db = new SQLiteAsyncConnection(path);

            db.CreateTableAsync<User>().Wait();
            
        }

        public async Task<int> AddUserAsync(User user)
        {
            if (await db.FindAsync<User>(x => x.Id.Equals(user.Id)) != null)
            {
                Console.WriteLine("DB: Updating user");
                // Käyttäjän tietojen päivittäminen
                return await db.UpdateAsync(user);
            }
            else
            {
                Console.WriteLine("DB: Creating user");
                // Käyttäjän lisääminen tietokantaan
                return await db.InsertAsync(user);
            }
        }

        public async Task<int> DelUserAsync(User user)
        {
            return await db.DeleteAsync(user);
        }

        public async Task<int> ClearTableAsync()
        {
            return await db.ExecuteAsync("DELETE FROM User");
        }

        public async Task<List<User>> GetUsersAsync()
        {
            return await db.Table<User>().ToListAsync();
        }

        public async Task<User> GetUserAsync(int id)
        {
            return await db.Table<User>().Where(i => i.Id == id).FirstOrDefaultAsync();
        }

        public async Task<User> GetUserAsync(string username)
        {
            return await db.Table<User>().Where(i => i.Username == username).FirstOrDefaultAsync();
        }

        public async Task<bool> CheckCredentialsAsync(string username, string password)
        {
            User user = await db.Table<User>().Where(usr => usr.Username.Contains(username)).FirstOrDefaultAsync();
            if (user != null)
            {
                if (user.Username.Equals(username) && user.Password.Equals(password))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public async Task<int> GetNextIdAsync()
        {
            // Haetaan käyttäjät
            List<User> users = await GetUsersAsync();

            // Haetaan käyttäjien identifikaatit integereinä
            List<int> ids = users.Select(x => x.Id).ToList();

            // Laskijan määritys
            int counter = 0;
            if (ids.Count() > 0)
            {
                counter = ids.First();
            } else
            {
                counter = counter - 1;
            }

            while (counter < 999)
            {
                // Jos id lista ei sisällä laskijaa
                if (!ids.Contains(counter++))
                {
                    // Palautetaan laskija (joka ei ole listassa)
                    return counter;
                }
            }

            // Palautetaan joku erroriksi merkitty numero vaikka koska en jaksa muuttaa tietokantaa hyväksymään nulleja :D
            return 404;
            
        }

        public async Task<bool> DoesUnExistAsync(string username)
        {
            List<User> users = await GetUsersAsync();

            List<string> usernames = users.Select(x => x.Username).ToList();

            if (usernames.Contains(username))
            {
                return true;
            } else
            {
                return false;
            }
        }
    }
}

