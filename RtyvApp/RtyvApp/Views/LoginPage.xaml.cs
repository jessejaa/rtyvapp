﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RtyvApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LoginPage : ContentPage
	{
		public LoginPage ()
		{
			InitializeComponent ();
            btnLogin.Clicked += OnLoginAsync;

            var lblRegTap = new TapGestureRecognizer();
            lblRegTap.Tapped += (s, e) =>
            {
                OnRegLblTap();
            };
            lblReg.GestureRecognizers.Add(lblRegTap);   
        }

        // Simppeli handleri buttonin painamiselle
        async void OnLoginAsync(object sender, EventArgs e)
        {
            // Tietojen tarkistus
            string un = entryUsername.Text;
            string pw = entryPassword.Text;

            bool isCorrect = await App.UserService.CheckCredentialsAsync(un, pw);

            if (isCorrect)
            {
                // Tallennetaan "CredentialServiceen" tunnukset / käyttäjä
                App.CurrentUser = await App.UserService.GetUserAsync(un);

                // Siirrytään päänäkymään ja vaihdetaan stackijärjestystä
                Application.Current.MainPage = new ShopPage();
            }
            else
            {
                await DisplayAlert("Huomio!", "Salasana tai käyttäjätunnus väärin", "OK");
            }
        }

        void OnRegLblTap()
        {
            App.Current.MainPage = new NavigationPage(new RegisterPage());
            //await Navigation.PushAsync(new RegisterPage());

        }
    }
}