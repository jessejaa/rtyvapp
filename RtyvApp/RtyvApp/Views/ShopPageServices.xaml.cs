﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RtyvApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ShopPageServices : ContentPage
	{
		public ShopPageServices ()
		{
			InitializeComponent ();
		}

        async void GetMeOutThanks(object sender, EventArgs e)
        {
            await DisplayAlert("RTYV App", "Tilaykset tehty ja käsitelty. Varmistus on lähetetty sähköpostiisi.", "Ok");
            await Navigation.PopToRootAsync();
        }
	}
}