﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using RtyvApp.Models;

namespace RtyvApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ShopPageMaster : ContentPage
    {
        public ListView ListView;

        public ShopPageMaster()
        {
            InitializeComponent();

            BindingContext = new ShopPageMasterViewModel();
            ListView = MenuItemsListView;
            
            var tapReg = new TapGestureRecognizer();
            tapReg.Tapped += (s, e) =>
            {
                OnLogout();
            };
            this.lblLogout.GestureRecognizers.Add(tapReg);

        }

        class ShopPageMasterViewModel : INotifyPropertyChanged
        {
            public ObservableCollection<ShopPageMenuItem> MenuItems { get; set; }
            
            public ShopPageMasterViewModel()
            {
                MenuItems = new ObservableCollection<ShopPageMenuItem>(new[]
                {
                    new ShopPageMenuItem { Id = 0, Title = "Listaukset", TargetType = typeof(ShopPageDetail) },
                    new ShopPageMenuItem { Id = 1, Title = "Omat listaukset", TargetType = typeof(ShopPageMy) },
                    new ShopPageMenuItem { Id = 2, Title = "Ostoskori", TargetType = typeof(ShopPageCart) },
                    new ShopPageMenuItem { Id = 3, Title = "Palvelut", TargetType = typeof(ShopPageServices) },
                    new ShopPageMenuItem { Id = 4, Title = "Profiili ja asetukset", TargetType = typeof(ShopPageProfileAndSettings) },
                });
            }
            
            #region INotifyPropertyChanged Implementation
            public event PropertyChangedEventHandler PropertyChanged;
            void OnPropertyChanged([CallerMemberName] string propertyName = "")
            {
                if (PropertyChanged == null)
                    return;

                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
            #endregion
        }

        async void OnLogout()
        {
            //await Navigation.PopAsync();
            App.Current.MainPage = new NavigationPage(new LoginPage());
        }
    }
}