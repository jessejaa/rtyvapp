﻿using RtyvApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RtyvApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ShopPageMy : ContentPage
	{
		public ShopPageMy ()
		{
			InitializeComponent ();

            //InitMyList();

            btnNew.Clicked += OnNewListingClick;

        }

        protected override void OnAppearing()
        {
            InitMyList();
        }

        async void OnNewListingClick(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new NewListingPage());
        }


        async void InitMyList()
        {
            // Haetaan lista omista myytävistä tuotteista
            List<ShopItem> myItems = await App.ItemService.GetShopItemsAsync(App.CurrentUser.Id);

            this.slMy.Children.Clear();

            // Populoidaan paikalliseen leiskaan
            PopulateStackLayout(this.slMy, myItems);
        }
        
        void PopulateStackLayout(StackLayout layout, List<ShopItem> shopItems)
        {
            foreach (ShopItem item in shopItems)
            {
                Uri uri;
                if (string.IsNullOrEmpty(item.Url))
                {
                    uri = new Uri("https://static.byggmax.com/media/!Item!7270058!productImage!0_7270058.jpg/category/Kannatin_Habo_97_.jpg");
                }
                else
                {
                    uri = new Uri(item.Url);
                }

                Image image = new Image
                {
                    Source = new UriImageSource { CachingEnabled = false, Uri = uri },
                    HorizontalOptions = LayoutOptions.Center
                };

                Label header = new Label
                {
                    Text = item.Name,
                    FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
                    HorizontalOptions = LayoutOptions.Center
                };

                Label info = new Label
                {
                    Text = item.Info,
                    FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                    HorizontalOptions = LayoutOptions.Center
                };

                Label count = new Label
                {
                    Text = "Myynnissä " + item.Count.ToString() + "kpl",
                    FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                    HorizontalOptions = LayoutOptions.Center
                };

                Label price = new Label
                {
                    Text = "Hinta: " + item.Price + " /kpl",
                    FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                    FontAttributes = FontAttributes.Bold,
                    HorizontalOptions = LayoutOptions.Center
                };

                Entry ecount = new Entry
                {
                    Placeholder = "Määrä",
                    WidthRequest = 50,
                    Keyboard = Keyboard.Numeric,
                    FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                    FontAttributes = FontAttributes.Bold,
                    HorizontalOptions = LayoutOptions.Center
                };

                BoxView line = new BoxView
                {
                    HeightRequest = 1,
                    WidthRequest = 100,
                    VerticalOptions = LayoutOptions.Center,
                    HorizontalOptions = LayoutOptions.Fill,
                    BackgroundColor = Color.SlateGray,
                };

                BoxView thickline = new BoxView
                {
                    HeightRequest = 2,
                    WidthRequest = 100,
                    VerticalOptions = LayoutOptions.Center,
                    HorizontalOptions = LayoutOptions.Fill,
                    BackgroundColor = Color.SlateGray,
                };

                Button btndel = new Button
                {
                    Text = "Poista",
                    VerticalOptions = LayoutOptions.Fill,
                    HorizontalOptions = LayoutOptions.Fill,
                    BackgroundColor = Color.FromHex("#8e0f0f"),
                    BorderColor = Color.FromHex("b71616"),
                    TextColor = Color.White,
                };

                btndel.Clicked += async (sender, e) =>
                {
                    int success = await App.ItemService.DelShopItemAsync(item);
                    if (success == 1)
                    {
                        await DisplayAlert("Listauspalvelu", "Tuote poistettu listauksista", "OK");
                        InitMyList();
                    }
                };

                Button btnedit = new Button
                {
                    Text = "Muokkaa [TODO]",
                    VerticalOptions = LayoutOptions.Fill,
                    HorizontalOptions = LayoutOptions.Fill,
                    BackgroundColor = Color.DarkGray,
                    BorderColor = Color.Gray,
                    TextColor = Color.White,
                };

                // TODO buttonille click metodi

                var itemLayout = new StackLayout();

                itemLayout.Children.Add(header);
                itemLayout.Children.Add(info);
                itemLayout.Children.Add(image);

                itemLayout.Children.Add(count);
                itemLayout.Children.Add(ecount);

                itemLayout.Children.Add(line);

                itemLayout.Children.Add(price);

                itemLayout.Children.Add(btnedit);
                itemLayout.Children.Add(btndel);

                itemLayout.Children.Add(thickline);


                layout.Children.Add(itemLayout);
            }
        }
    }
}