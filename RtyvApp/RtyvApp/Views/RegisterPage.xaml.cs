﻿using RtyvApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RtyvApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RegisterPage : ContentPage
	{
		public RegisterPage ()
		{
			InitializeComponent ();

            this.btnRegister.Clicked += OnRegAsync;
            this.btnCancel.Clicked += OnCancelAsync;
        }

        async void OnRegAsync(object sender, EventArgs e)
        {
            int id = await App.UserService.GetNextIdAsync();
            string fn = entryFn.Text;
            string ln = entryLn.Text;
            string un = entryUn.Text;
            string pw = entryPw1.Text;

            if (!string.IsNullOrEmpty(fn) && !string.IsNullOrEmpty(ln) && !string.IsNullOrEmpty(un))
            {
                if (entryPw1.Text.Contains(entryPw2.Text))
                {
                    if (await App.UserService.DoesUnExistAsync(un) == false)
                    {
                        // Luodaan käyttäjä kaikkien ehtolauseiden jälkeen
                        User u = new User
                        {
                            Id = id,
                            Firstname = fn,
                            Lastname = ln,
                            Username = un,
                            Password = pw
                        };

                        int success = await App.UserService.AddUserAsync(u);

                        if (success == 1)
                        {
                            u = new User();

                            await DisplayAlert("Käyttäjäpalvelu", "Käyttäjän luominen onnistui", "OK");

                            await Navigation.PopAsync();
                            App.Current.MainPage = new LoginPage();
                        }
                        else
                        {
                            await DisplayAlert("Käyttäjäpalvelu", "Käyttäjän luominen epäonnistui", "OK");
                        }
                    } else
                    {
                        await DisplayAlert("Käyttäjäpalvelu", "Käyttäjänimi jo käytössä", "OK");
                    }
                } else
                {
                    await DisplayAlert("Käyttäjäpalvelu", "Salasanat eivät täsmää", "OK");
                }
            } else
            {
                await DisplayAlert("Käyttäjäpalvelu", "Täytä tarvittavat tiedot", "OK");
            }

        }

        async void OnCancelAsync(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
            App.Current.MainPage = new LoginPage();
        }
	}
}