﻿using RtyvApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RtyvApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ShopPageCart : ContentPage
	{
		public ShopPageCart ()
		{
			InitializeComponent ();

            //InitCart();

            btnOrder.Clicked += OnOrder;
		}

        protected override void OnAppearing()
        {
            InitCart();
        }

        void InitCart()
        {
            this.slCart.Children.Clear();
            PopulateCart(this.slCart, App.Cart);
        }

        void PopulateCart(StackLayout layout, List<ShopItem> list)
        {
            foreach (ShopItem item in list)
            {
                StackLayout itemLayout = new StackLayout();

                Label header = new Label
                {
                    Text = item.Name,
                    FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
                    HorizontalOptions = LayoutOptions.Start
                };

                Label count = new Label
                {
                    Text = item.Count.ToString() + " kpl",
                    FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                    HorizontalOptions = LayoutOptions.Start
                };

                Button del = new Button
                {
                    Text = "Poista",
                    HorizontalOptions = LayoutOptions.End,
                    BackgroundColor = Color.FromHex("#8e0f0f"),
                    BorderColor = Color.FromHex("b71616"),
                    TextColor = Color.White,
                };

                del.Clicked += (sender, e) =>
                {
                    App.Cart.Remove(item);
                    //layout.Children.Remove(itemLayout);
                    InitCart();
                };

                BoxView thickline = new BoxView
                {
                    HeightRequest = 2,
                    WidthRequest = 100,
                    VerticalOptions = LayoutOptions.Center,
                    HorizontalOptions = LayoutOptions.Fill,
                    BackgroundColor = Color.SlateGray,
                };
                
                itemLayout.Children.Add(header);
                itemLayout.Children.Add(count);
                itemLayout.Children.Add(del);
                itemLayout.Children.Add(thickline);

                layout.Children.Add(itemLayout);
            }

        }

        async void OnOrder(object sender, EventArgs e)
        {
            await DisplayAlert("Rtyv App", "Tilaus käsitellään, jatka kuljetuspavlelun valintaan.", "Ok");

            slCart.Children.Clear();

            await Navigation.PushAsync(new ShopPageServices());
            
        }
	}
}