﻿using RtyvApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RtyvApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class NewListingPage : ContentPage
	{
		public NewListingPage ()
		{
			InitializeComponent ();
            this.btnSave.Clicked += OnSaveClick;
		}

        async void OnSaveClick(object sender, EventArgs e)
        {
            ShopItem item = new ShopItem
            {
                Id = await App.ItemService.GetNextIdAsync(),
                SellerID = App.CurrentUser.Id,
                Name = this.entryIname.Text,
                Info = this.entryIextra.Text,
                OnSale = true,
                Count = Int32.Parse(this.entryIcount.Text),
                Price = Int32.Parse(this.entryIprice.Text),
                Url = "",
            };

            int success = await App.ItemService.AddShopItemAsync(item);
            if (success == 1)
            {
                await DisplayAlert("Listauspalvelu", "Tuotteen listaaminen onnistui!", "OK");
                await Navigation.PopAsync();
            } else
            {
                await DisplayAlert("Listauspalvelu", "Tuotteen listaaminen epäonnistui...", "OK");

            }
        }
	}
}