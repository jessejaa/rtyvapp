﻿using RtyvApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RtyvApp.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ShopPageProfileAndSettings : ContentPage
	{
		public ShopPageProfileAndSettings ()
		{
			InitializeComponent ();

            InitProfileSettingsPage();
        }

        void InitProfileSettingsPage()
        {
            this.lblHeader.Text = "Käyttäjän " + App.CurrentUser.Username + " [ID=" + App.CurrentUser.Id.ToString() + "] asetukset";

            this.entryFn.Text = App.CurrentUser.Firstname;
            this.entryLn.Text = App.CurrentUser.Lastname;

            this.entryUn.Text = App.CurrentUser.Username;
            this.entryUn.IsEnabled = false;

            this.entryPw1.Text = App.CurrentUser.Password;
            this.entryPw2.Text = App.CurrentUser.Password;

            this.btnSave.Clicked += async (sender, e) =>
            {
                if (!string.IsNullOrEmpty(this.entryFn.Text) && !string.IsNullOrEmpty(this.entryLn.Text) && !string.IsNullOrEmpty(this.entryUn.Text))
                {
                    if (entryPw1.Text.Contains(entryPw2.Text))
                    {
                        User userObj = new User
                        {
                            Id = App.CurrentUser.Id,
                            Firstname = this.entryFn.Text,
                            Lastname = this.entryLn.Text,
                            Username = this.entryUn.Text,
                            Password = this.entryPw1.Text
                        };

                        // Sama metodi myös päivittää kuin tekee uuden käyttäjän
                        int success = await App.UserService.AddUserAsync(userObj);

                        if (success == 1)
                        {
                            App.CurrentUser = userObj;
                            userObj = new User();
                            await DisplayAlert("Käyttäjäpalvelu", "Käyttäjän päivittäminen onnistui", "OK");
                        } else
                        {
                            await DisplayAlert("Käyttäjäpalvelu", "Käyttäjän päivittäminen epäonnistui", "OK");
                        }
                    } else
                    {
                        await DisplayAlert("Käyttäjäpalvelu", "Salasanat eivät täsmää", "OK");
                    }
                } else
                {
                    await DisplayAlert("Käyttäjäpalvelu", "Vaaditut tiedot eivät voi olla tyhjiä", "OK");
                }
            };
        }
    }
}