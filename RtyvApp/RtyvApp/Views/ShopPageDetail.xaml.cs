﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using RtyvApp.Models;
using RtyvApp.Views;

namespace RtyvApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ShopPageDetail : ContentPage
    {
        public ShopPageDetail()
        {
            InitializeComponent();

            //InitializeAsyncMethods();
        }

        protected override void OnAppearing()
        {
            InitializeAsyncMethods();
        }

        async void InitializeAsyncMethods()
        {
            // Haetaan tietokannasta lista kaikista myytävistä tuotteista
            List<ShopItem> shopItems = await App.ItemService.GetShopItemsAsync();

            this.slItems.Children.Clear();

            // Populoidaan stacklayout haetuista tuotteista
            PopulateStackLayout(this.slItems, shopItems);
        }

        void PopulateStackLayout(StackLayout layout, List<ShopItem> shopItems)
        {
            foreach(ShopItem item in shopItems)
            {
                var itemLayout = new StackLayout();

                Uri uri;
                if (string.IsNullOrEmpty(item.Url))
                {
                    uri = new Uri("https://static.byggmax.com/media/!Item!7270058!productImage!0_7270058.jpg/category/Kannatin_Habo_97_.jpg");
                }
                else
                {
                    uri = new Uri(item.Url);
                }

                Image image = new Image
                {
                    Source = new UriImageSource { CachingEnabled = false, Uri = uri },
                    HorizontalOptions = LayoutOptions.Center
                };

                Label header = new Label
                {
                    Text = item.Name,
                    FontSize = Device.GetNamedSize(NamedSize.Large, typeof(Label)),
                    HorizontalOptions = LayoutOptions.Center
                };

                Label info = new Label
                {
                    Text = item.Info,
                    FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
                    HorizontalOptions = LayoutOptions.Center
                };

                Label count = new Label
                {
                    Text = "Myynnissä " + item.Count.ToString() + "kpl",
                    FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
                    HorizontalOptions = LayoutOptions.Center
                };

                Label price = new Label
                {
                    Text = "Hinta: " + item.Price + " /kpl",
                    FontSize = Device.GetNamedSize(NamedSize.Small, typeof(Label)),
                    FontAttributes = FontAttributes.Bold,
                    HorizontalOptions = LayoutOptions.Center
                };

                Entry ecount = new Entry
                {
                    Placeholder = "Määrä",
                    WidthRequest = 100,
                    Keyboard = Keyboard.Numeric,
                    FontSize = Device.GetNamedSize(NamedSize.Medium, typeof(Label)),
                    FontAttributes = FontAttributes.Bold,
                    HorizontalOptions = LayoutOptions.Center
                };

                BoxView line = new BoxView
                {
                    HeightRequest = 1,
                    WidthRequest = 100,
                    VerticalOptions = LayoutOptions.Center,
                    HorizontalOptions = LayoutOptions.Fill,
                    BackgroundColor = Color.SlateGray,
                };

                BoxView thickline = new BoxView
                {
                    HeightRequest = 4,
                    WidthRequest = 100,
                    VerticalOptions = LayoutOptions.Center,
                    HorizontalOptions = LayoutOptions.Fill,
                    BackgroundColor = Color.SlateGray,
                };
                
                Button button = new Button
                {
                    Text = "Lisää ostoskoriin",
                    VerticalOptions = LayoutOptions.Fill,
                    HorizontalOptions = LayoutOptions.Fill
                };

                button.Clicked += (sender, e) =>
                {
                    int cartCount = 1;
                    if (!string.IsNullOrEmpty(ecount.Text))
                    {
                        cartCount = Int32.Parse(ecount.Text);
                    }

                    item.Count = item.Count - cartCount;
                    count.Text = "Myynnissä " + item.Count.ToString() + "kpl";

                    bool sale = true;
                    if (item.Count <= 0)
                    {
                        sale = false;
                    }

                    ShopItem cartItem = new ShopItem
                    {
                        Id = item.Id,
                        SellerID = item.SellerID,
                        Name = item.Name,
                        Info = item.Info,
                        Count = cartCount,
                        OnSale = sale,
                        Price = item.Price,
                        Url = item.Url,
                    };

                    App.Cart.Add(cartItem);

                    if (sale == false)
                    {
                        layout.Children.Remove(itemLayout);
                    };

                };
                
                itemLayout.Children.Add(header);
                itemLayout.Children.Add(info);
                itemLayout.Children.Add(image);

                itemLayout.Children.Add(count);
                itemLayout.Children.Add(ecount);

                itemLayout.Children.Add(line);

                itemLayout.Children.Add(price);
                itemLayout.Children.Add(button);

                itemLayout.Children.Add(thickline);


                layout.Children.Add(itemLayout);
            }
        }
    }
}