﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

using RtyvApp.Views;
using RtyvApp.Services;
using RtyvApp.Interfaces;
using RtyvApp.Models;

namespace RtyvApp
{
	public partial class App : Application
	{
        // Palvelut (mukamas verkon välityksellä toimivat palvelut)
        public static UserService userService;
        public static ShopItemService itemService;

        // CredentialsService, mutta käytetään protossa vain globaaleja muuttujia
        public static User CurrentUser { get; set; }

        public static List<ShopItem> Cart { get; set; }

        public App ()
		{
			InitializeComponent();

            CurrentUser = new User();
            Cart = new List<ShopItem>();

            DBdata();

            MainPage = new LoginPage();
		}

        async void DBdata()
        {
            // Tyhjennä tietokannan data
            //int usersclear = await UserService.ClearTableAsync();
            //int itemsclear = await ItemService.ClearTableAsync();

            // Lisätään vain jotain dataa tietokantaan
            int usr = await UserService.AddUserAsync(new User
            {
                Id = 0,
                Firstname = "Kalle",
                Lastname = "Kokeilija",
                Username = "kalle",
                Password = "kalle"
            });
            usr = await UserService.AddUserAsync(new User
            {
                Id = 1,
                Firstname = "Tero",
                Lastname = "Testaaja",
                Username = "tero",
                Password = "tero"
            });

            int itm = await ItemService.AddShopItemAsync(new ShopItem
            {
                Id = 0,
                SellerID = 0,

                Name = "Kattopelti, sinkitty",
                Info = "T20 Standard -kattopelti on muotoilultaan suoralinjaisen tyylikäs ja se soveltuu useaan eri käyttötarkoitukseen. Vesikatteen tasalinjainen porrastus tekee siitä näyttävän ja helpon asentaa. Sinkitty.",
                Price = 8.77,
                Count = 220,

                OnSale = true,

                Url = "https://media.taloon.com/image/upload/q_70,f_auto,w_1200,h_1200,c_limit/cloud/k/ruukki/ruukki_kattopelti_t20_sinkitty.jpg"
            });
            itm = await ItemService.AddShopItemAsync(new ShopItem
            {
                Id = 1,
                SellerID = 0,

                Name = "Kestopuu A123",
                Info = "Mitallistettu kestopuu eli painekyllästetty puutavara on mittatarkaksi karkeahöylättyä sahatavaraa. Käytetään mm. terassien ja muiden ulos jäävien rakenteiden kantavana rakenteena. Visakoivua.",
                Price = 124.55,
                Count = 2,

                OnSale = true,

                Url = "https://media.taloon.com/image/upload/q_70,f_auto,w_1200,h_1200,c_limit/cloud/k/puumerkki/FI00083_Kyllastetty_A_manty_MITAL_48x148.jpg"
            });


        }

        public static UserService UserService
        {
            get
            {
                if (userService == null)
                {
                    userService = new UserService(DependencyService.Get<ILocalDB>().DBConn("RtyvDB.db3"));
                }
                return userService;
            }
        }

        public static ShopItemService ItemService
        {
            get
            {
                if (itemService == null)
                {
                    itemService = new ShopItemService(DependencyService.Get<ILocalDB>().DBConn("RtyvDB.db3"));
                }
                return itemService;
            }
        }

        protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
