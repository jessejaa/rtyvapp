﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RtyvApp.Interfaces
{
    // Luodaan rajapinta jotta voidaan käyttää laitekohtaisia tietokantoja
    public interface ILocalDB
    {
        string DBConn(string filename);
    }
}
