﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RtyvApp.Models
{
    public class ShopItem
    {
        [SQLite.PrimaryKey]
        public int Id { get; set; }
        public int SellerID { get; set; }

        public string Name { get; set; }
        public string Info { get; set; }
        public double Price { get; set; }
        public int Count { get; set; }

        public bool OnSale { get; set; }

        public string Url { get; set; }
    }
}
