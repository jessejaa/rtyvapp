﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RtyvApp.Views
{

    public class ShopPageMenuItem
    {
        public ShopPageMenuItem()
        {
            TargetType = typeof(ShopPageDetail);
        }
        public int Id { get; set; }
        public string Title { get; set; }

        public Type TargetType { get; set; }
    }
}