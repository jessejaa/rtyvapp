﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RtyvApp.Models
{
    // Tämä luokka toimii paikalliselle tietokannalle taulun mallina
    public class User
    {
        [SQLite.PrimaryKey]
        public int Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        //SQLite ei tue listoja joten emme voi käyttää tätä
        //public List<ShopItem> ShopItems { get; set; }
    }
}
